#include <stdlib.h>
#include <stdio.h>

struct effect;

struct object {
    struct effect *effh;
    struct effect *efft;
};

/* 
   Effects. Used to create temporary anomalies in the game world that can be made to last a given time.
   Could be used for anything from animations, to characters burning, to powerups, anything. 
*/
struct effect {
    int (*t)(struct object *, void *);
    void *d;
    void (*f)(void *);

    struct effect *next;
    struct effect *prev;
};

void 
deleteeffect(struct effect *e);

void
unlinkeffect(struct object *o, struct effect *e);

struct object*
createobject()
{
    struct object *o = (struct object*)calloc(sizeof(struct object), 1);
    return o;
}

void 
doeffects(struct object *o)
{
    void *d;
    int (*t)(struct object *, void *);

    struct effect *i = o->effh, *p;
    while (i) {
	t = i->t;
	d = i->d;

	p = i;
	i = i->next;

	if (t(o, d)) {
	    unlinkeffect(o, p);
	    deleteeffect(p); 
	}
    }
}

void 
addeffect(struct object *o, int (*t)(struct object *, void *), void (*f)(void *), void *d)
{
    struct effect *e = (struct effect*)malloc(sizeof(struct effect));

    e->t = t;
    e->d = d;
    e->f = f;

    if (o->efft) {
	o->efft->next = e;
	e->prev = o->efft;
    } else {
	o->effh = e;
	e->prev = NULL;
    }

    o->efft = e;
    e->next = NULL;
}

void
modifyeffects(struct object *o, int (*m)(int (*)(struct object *, void *), void *))
{
    void *d;
    int (*t)(struct object *, void *);
    int c;    

    struct effect *i = o->effh, *p;
    while (i) {
	t = i->t;
	d = i->d;

	c = m(t, d);

	p = i;
	i = i->next;

	if (c) {
	    unlinkeffect(o, p);
	    deleteeffect(p);
	    if (c < 0) return;
	}
    }
}

void
unlinkeffect(struct object *o, struct effect *e)
{
    if (e == o->effh) o->effh = o->effh->next;
    if (e == o->efft) o->efft = o->efft->prev;
}

/* Unlinks and deletes the effect from the target object. */
void 
deleteeffect(struct effect *e)
{
    if (e->prev) e->prev->next = e->next;
    if (e->next) e->next->prev = e->prev;

    void (*f)(void *) = e->f;
    void *d = e->d;

    if (f) f(d);

    free(e);
}

void
clearobject(struct object *o)
{
    struct effect *curr = o->effh, *next;
    while (curr) {
	next = curr->next;
	deleteeffect(curr);
	curr = next;
    }

    o->effh = NULL;
    o->efft = NULL;
}

void
deleteobject(struct object *o)
{
    clearobject(o);
    free(o);
}
