#include "eff.h"
#include <stdio.h>
#include <stdlib.h>

struct helloworld
{
    float num;
    int i;
    const char *str;
};

struct helloworld*
helloworld_create(float num, int i, const char *str)
{
    struct helloworld *out = (struct helloworld*)malloc(sizeof(struct helloworld));
    out->num = num;
    out->i   = i;
    out->str = str;
    return out;
}

static int run = 1;

int 
helloworld(struct object *o, void *d)
{
    struct helloworld *c = (struct helloworld*)(d);
    printf("%s(%6.2f)!\n", c->str, c->num);

    if (--(c->i) == 0) {
	run = 0;
	return 2;
    } else {
	return 0;
    }
}

int
badone(struct object *o, void *d)
{
    return 0;
}

int
goodone(struct object *o, void *d)
{
    return 0;
}

int
removebadones(int (*t)(struct object *, void *d), void *d)
{
    int c = (t == badone);

    if (c)
	puts("Bad one found!");
    else 
	puts("Good one found!");

    return c;
}


int 
main(int argc, const char *argv[])
{
    struct object *object = createobject();

    void *data = helloworld_create(4.2f, 6, "Hello, worldspread before!");

    addeffect(object, helloworld, free, data);
    
    while (run) {
	doeffects(object);
	run++;
    }

    addeffect(object, goodone, NULL, NULL);
    addeffect(object, goodone, NULL, NULL);
    addeffect(object, badone, NULL, NULL);

    modifyeffects(object, removebadones);

    return 0;
}
