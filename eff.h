struct object;

struct object*
createobject();

void
deleteobject(struct object *o);

/* Create a new effect for the given object.
   O is the object to attach this effect to.
   T is a function-pointer that does the effect.
   D is data for the function-pointer.
   F is a function-pointer for freeing the effect-data at the end of execution. 
       \ Leave this NULL is D was declared on the stack, or use free() if your data structure is contiguous.

   Return non-zero on the update function(t) to end execution and free() your effect structure.
*/
void 
addeffect(struct object *o, int (*t)(struct object *, void *), void (*f)(void *), void *d);

/* Parses the effects attached to this object with a function.
   O is the object to parse.
   M is the function-pointer that will parse the attached effects.

   Have M return  0 to continue parsing.
   Have M return <0 to delete the current effect and stop parsing.
   Have M return >0 to delete the current effect and continue parsing.
*/
void
modifyeffects(struct object *o, int (*m)(int (*)(struct object *, void *), void *));

void 
doeffects(struct object *o);
