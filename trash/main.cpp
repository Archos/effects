#include <iostream>
#include <list>

static int run = 1;

class Effect
{
    protected:
    Object &obj;

    public:
    Effect(Object &a_obj): obj(a_obj) { }
    virtual bool Tick() = 0;
    virtual ~Effect() { 
	std::cout << "Destructor called!" << std::endl;
    }
}

class HellWorld: public Effect
{
    private:
    int &circle;

    public:
    HellWorld(int &a_circle): circle(a_circle) { }

    bool Tick() {
	const char *num;
	switch (circle--) {
	    case 1: num = "st"; break;
	    case 2: num = "nd"; break;
	    case 3: num = "rd"; break;
	    default: 
		num = "th";
	}

	std::cout << "Welcome to the " << circle << num << " circle of hell!" << std::endl;
	return (circle < 1);
    }
}

class Object
{
    public:
    std::list<Effect*> effects;

    Object() { effects = new std::list<Effect*>(); }

    void UpdateEffects() {
	for (std::list<Effect*>::iterator i = effects.begin(); i != effects.end(); ++i) {
	    if (i->Tick()) effects.Remove(*i);
	}
    }
}

int main()
{
    int a = 5;
    Object object;
    object.effects.Add(new HellWorld(a));
}
